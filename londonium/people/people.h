/****************************************************************

    people.h

    =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

    This software is a continuing work of Tom Barbalet, begun on
    13 June 1996. No apes or cats were harmed in the writing of
    this software.

****************************************************************/

#include "../../apesdk/toolkit/toolkit.h"
#include "../../apesdk/cli/cli.h"

#ifndef LONDINIUM_PEOPLE_CLI_H
#define LONDINIUM_PEOPLE_CLI_H

#define CHROMOSOMES                 4
#define CHROMOSOME_Y                0

typedef n_byte4 n_genetics;

typedef struct
{
    n_int       location_x, location_y, location_z;
    n_byte4     date_of_birth;
    n_genetics  genetics[CHROMOSOMES];
    n_byte4     occupation;
} simulated_person;

// n_int number_of_children[20] = {0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 4, 4, 5, 6, 6};

#define PEOPLE_NUMBER (10000000)

/*
 Two grand mothers
 
 Six daughters parents
 
 Thirty six daughters
 
 44 People to People Units (Three generations)

 1931818 People Units (Blend of males and females all full)
 
 84999992 Total Population
 */


void people_previous(void * ptr, n_console_output output_function);

void people_next(void * ptr, n_console_output output_function);

n_int people_cycle(void * ptr);

void people_watch_entity( void *ptr, n_console_output output_function );

n_int people_init(n_byte2 *local);


#endif
