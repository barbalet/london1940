/****************************************************************

    tosh.c

    =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

    This software is a continuing work of Tom Barbalet, begun on
    13 June 1996. No apes or cats were harmed in the writing of
    this software.

****************************************************************/

#include "../../apesdk/toolkit/toolkit.h"
#include "../../apesdk/cli/cli.h"

#include "../people/people.h"
#include "../place/place.h"

#include <stdio.h>


n_int draw_error( n_constant_string error_text, n_constant_string location, n_int line_number )
{
    printf( "ERROR: %s @ %s %ld\n", ( const n_string ) error_text, location, line_number );
    return -1;
}


n_int tosh_generic( void *ptr, n_string response, n_console_output output_function )
{
    return 0;
}

const static simulated_console_command tosh_commands[] =
{
    {&io_help,               "help",           "[(command)]",          "Displays a list of all the commands"},

    {&useful_quit,           "quit",           "",                     "Quits the console"},
    {&useful_quit,           "exit",           "",                     ""},
    {&useful_quit,           "close",          "",                     ""},

    {&useful_stop,          "stop",           "",                     "Stop the simulation during step or run"},
    {&useful_run,           "run",            "(time format)|forever", "Simulate for a given number of days or forever"},
    {&useful_step,          "step",           "",                     "Run for a single logging interval"},
    {&useful_interval,      "interval",       "(days)",               "Set the simulation logging interval in days"},
    {&useful_simulation,    "simulation",     "",                     ""},
    {&useful_simulation,    "sim",            "",                     "Show simulation parameters"},

    {&useful_next,          "next",           "",                     "Next person"},

    {&useful_previous,      "previous",       "",                     "Previous person"},
    {&useful_previous,      "prev",           "",                     ""},

    {0L, 0L},
};





int main(int argc, const char * argv[]) {
    
    n_byte2 local[2]={0x6737, 0x6283};
    
    (void)people_init((n_byte2 *)local);
    
    cli_set_previous_next(people_previous, people_next);
    cli_set_simulation(place_simulation);
    cli_set_cycle_entity(people_cycle, people_watch_entity);
    
    do
    {}
    while ( io_console( 0L,
                        (simulated_console_command *)tosh_commands,
                       io_console_entry,
                       io_console_out ) == 0 );
    return 0;
}
